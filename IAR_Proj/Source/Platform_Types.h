#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H

#define TRUE 1
#define FALSE 0

typedef unsigned char boolean;
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
typedef unsigned long long uint64;
typedef signed char sint8;
typedef signed short sint16;
typedef signed long sint32;
typedef signed long long sint64;


typedef uint8 Std_ReturnType;

#define NULL_PTR ((void *)0)


#endif
