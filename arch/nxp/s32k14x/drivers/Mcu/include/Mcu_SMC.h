/**
*   @file    Mcu_SMC.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Function prototypes.
*   @details Interface available for IPW layer only.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_SMC_H
#define MCU_SMC_H


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_SMC_Types.h"
#include "Mcu_EnvCfg.h"

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


#if (defined(MCU_DISABLE_SMC_INIT) && (STD_OFF == MCU_DISABLE_SMC_INIT))
#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SMC_AllowedModesConfig(SMC_pConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SMC_AllowedModesConfig,(SMC_pConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SMC_AllowedModesConfig(SMC_pConfigPtr) \
do\
{ \
        Mcu_SMC_AllowedModesConfig(SMC_pConfigPtr); \
}\
while(0)
#endif
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SMC_ModeConfig(Mcu_pModeConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SMC_ModeConfig,(Mcu_pModeConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SMC_ModeConfig(Mcu_pModeConfigPtr) \
do\
{ \
        Mcu_SMC_ModeConfig(Mcu_pModeConfigPtr); \
}\
while(0)
#endif
#endif


#if (defined(MCU_DISABLE_SMC_INIT) && (STD_OFF == MCU_DISABLE_SMC_INIT))
FUNC( void, MCU_CODE) Mcu_SMC_AllowedModesConfig(P2CONST(Mcu_SMC_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
#endif

FUNC( void, MCU_CODE) Mcu_SMC_ModeConfig(P2CONST(Mcu_ModeConfigType, AUTOMATIC, MCU_APPL_CONST) pModeConfigPtr);

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

#endif /* MCU_SMC_H */

/** @} */
