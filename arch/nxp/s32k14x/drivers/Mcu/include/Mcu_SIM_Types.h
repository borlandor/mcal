/**
*   @file    Mcu_SIM_Types.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Exported data outside of the Mcu from IPV_SIM.
*   @details Public data types exported outside of the Mcu driver.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_SIM_TYPES_H
#define MCU_SIM_TYPES_H


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_Cfg.h"
#include "Soc_Ips.h"

/**
* @brief   SIM IP configuration.
* @details This structure contains information for Flash, ADC and FTM general options
*/
typedef struct
{
   /* Chip control configuration */
    VAR (uint32, MCU_VAR)  u32ChipControlConfiguration;
   /* LPO Clock Config */
    VAR (uint32, MCU_VAR)  u32LPOClockConfiguration;
   /* ADC configuration */
    VAR (uint32, MCU_VAR)  u32AdcOptionsConfiguration;
   /* FTMOPT0 Config */
    VAR (uint32, MCU_VAR)  u32FTMOPT0Configuration;
   /* FTMOPT1 configuration */
    VAR (uint32, MCU_VAR)  u32FTMOPT1Configuration;
   /* Flash configuration */
    VAR (uint32, MCU_VAR)  u32MiscellaneousConfiguration0;
   /* Miscellaneous Control configuration */
    VAR (uint32, MCU_VAR)  u32MiscellaneousConfiguration1;
} Mcu_SIM_ConfigType;

#if (MCU_INIT_CLOCK == STD_ON)
/**
* @brief            The structure contains SIM clock related configuration
* @details          Specifies the system behaviour during the selected clock configuration.
*                   Data set and configured by Mcu_InitClock call.
*/
typedef struct
{
   /* SIM_CHIPCTL Config */
    VAR (uint32, MCU_VAR)  u32ChipControlClockConfiguration;
   /* SIM_CLKDIV4 Config */
    VAR (uint32, MCU_VAR)  u32TraceClockConfiguration;
   /* Clock Gating Config */
    VAR (uint32, MCU_VAR)  u32ClockGatingConfiguration;
} Mcu_SIM_ClockConfigType;
#endif


#ifdef __cplusplus
}
#endif

#endif /* MCU_SIM_TYPES_H */

/** @} */
