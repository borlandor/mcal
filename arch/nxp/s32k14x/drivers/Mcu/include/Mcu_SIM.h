/**
*   @file    Mcu_SIM.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Function prototypes.
*   @details Interface available for IPW layer only.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_SIM_H
#define MCU_SIM_H


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_SIM_Types.h"
#include "Mcu_EnvCfg.h"


/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#define  Call_Mcu_SIM_Init(SIM_pConfigPtr)				Mcu_SIM_Init(SIM_pConfigPtr)
#define  Call_Mcu_SIM_ClockConfig(SIM_pClockConfigPtr)	Mcu_SIM_ClockConfig(SIM_pClockConfigPtr)
#define  Call_Mcu_SIM_SRAMRetentionConfig(bSRAMReten)	Mcu_SIM_SRAMRetentionConfig(bSRAMReten)


#if (defined(MCU_DISABLE_SIM_INIT) && (STD_OFF == MCU_DISABLE_SIM_INIT))
FUNC( void, MCU_CODE) Mcu_SIM_Init(P2CONST(Mcu_SIM_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
#endif

#if (MCU_INIT_CLOCK == STD_ON)
FUNC( void, MCU_CODE) Mcu_SIM_ClockConfig(P2CONST(Mcu_SIM_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pClockConfigPtr);
#endif

#ifdef MCU_SRAM_RETEN_CONFIG_API
#if (MCU_SRAM_RETEN_CONFIG_API == STD_ON)
FUNC( void, MCU_CODE ) Mcu_SIM_SRAMRetentionConfig(VAR (Mcu_SRAMRetenConfigType, AUTOMATIC) eSRAMRetenConfig);
#endif
#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

#endif /* MCU_SIM_H */

/** @} */
