/**
*   @file    Mcu_CortexM4.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - ARM CortexM4 Registers and Macros Definitions.
*   @details ARM CortexM4 Registers and Macros Definitions.
*
*   @addtogroup DMA_DRIVER
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/

/**
@file        Mcu_CortexM4.h
*/


#ifndef MCU_CORTEXM4_H
#define MCU_CORTEXM4_H


#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_Cfg.h"
#include "Reg_eSys_CortexM4.h"


#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"




#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
/** @violates @ref Mcu_CortexM4_h_REF_3 Function-like macro defined.*/
#define  Call_Mcu_CM4_EnableDeepSleep() \
do\
{ \
        Mcal_Trusted_Call(Mcu_CM4_EnableDeepSleep); \
}\
while(0)
    
/** @violates @ref Mcu_CortexM4_h_REF_3 Function-like macro defined.*/
#define  Call_Mcu_CM4_DisableDeepSleep() \
do\
{ \
        Mcal_Trusted_Call(Mcu_CM4_DisableDeepSleep); \
}\
while(0)
#else
/** @violates @ref Mcu_CortexM4_h_REF_3 Function-like macro defined.*/
#define  Call_Mcu_CM4_EnableDeepSleep() \
do\
{ \
        Mcu_CM4_EnableDeepSleep(); \
}\
while(0)
    
/** @violates @ref Mcu_CortexM4_h_REF_3 Function-like macro defined.*/
#define  Call_Mcu_CM4_DisableDeepSleep() \
do\
{ \
        Mcu_CM4_DisableDeepSleep(); \
}\
while(0)
#endif
#endif



#if (MCU_PERFORM_RESET_API == STD_ON)

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_CM4_SystemReset() \
do\
{ \
        Mcal_Trusted_Call(Mcu_CM4_SystemReset); \
}\
while(0)
#else
#define  Call_Mcu_CM4_SystemReset() \
do\
{ \
        Mcu_CM4_SystemReset(); \
}\
while(0)
#endif
#endif

#endif /* (MCU_PERFORM_RESET_API == STD_ON) */


#if (MCU_PERFORM_RESET_API == STD_ON)
 /**
* @brief        The function initiates a system reset request to reset the SoC.
* @details      The function initiates a system reset request to reset the SoC
*
* @param[in]    none
*
* @return void
*
*/
FUNC(void, MCU_CODE) Mcu_CM4_SystemReset(void);

#endif


FUNC(void, MCU_CODE) Mcu_CM4_EnableDeepSleep(void);
FUNC(void, MCU_CODE) Mcu_CM4_DisableDeepSleep(void);


#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"

#ifdef __cplusplus
}
#endif


#endif /* MCU_CORTEXM4_H */

/** @} */
