/**
*   @file    Mcu_PMC.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Function prototypes.
*   @details Interface available for IPW layer only.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_PMC_H
#define MCU_PMC_H


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_PMC_Types.h"
#include "Mcu_EnvCfg.h"


/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/
#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#if (STD_ON == MCU_PMC_REG_PROT_AVAILABLE)

#ifdef IPV_REG_PROT
#if (IPV_REG_PROT == IPV_REG_PROT_00_01_02_04)
/**
* @brief Size memory protected of CMU.
*/
#define PMC_PROT_MEM_U32                    ((uint32)0x00000001u)
#else
/**
* @brief Size memory protected of CMU.
*/
#define PMC_PROT_MEM_U32                    ((uint32)0x00000010u)
#endif
#else
/**
* @brief Size memory protected of CMU.
*/
#define PMC_PROT_MEM_U32                    ((uint32)0x00000010u)
#endif

#endif /* (STD_ON == MCU_PMC_REG_PROT_AVAILABLE) */
#endif /* (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT) */
#endif /* MCU_ENABLE_USER_MODE_SUPPORT */

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#if (STD_ON == MCU_PMC_REG_PROT_AVAILABLE)
FUNC( void, MCU_CODE) Mcu_PMC_SetUserAccessAllowed(void);
#endif /* (STD_ON == MCU_PMC_REG_PROT_AVAILABLE) */
#endif /* (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT) */
#endif /* MCU_ENABLE_USER_MODE_SUPPORT */

#if (defined(MCU_DISABLE_PMC_INIT) && (STD_OFF == MCU_DISABLE_PMC_INIT))
#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_PMC_PowerInit(PMC_pConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_PMC_PowerInit,(PMC_pConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_PMC_PowerInit(PMC_pConfigPtr) \
do\
{ \
        Mcu_PMC_PowerInit(PMC_pConfigPtr); \
}\
while(0)
#endif
#endif
#endif


#if (defined(MCU_DISABLE_PMC_INIT) && (STD_OFF == MCU_DISABLE_PMC_INIT))
FUNC( void, MCU_CODE) Mcu_PMC_PowerInit(P2CONST(Mcu_PMC_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
#endif

#if (MCU_VOLTAGE_ERROR_ISR_USED == STD_ON)
FUNC( void, MCU_CODE) Mcu_PMC_VoltageErrorIsr(VAR( void, AUTOMATIC));
FUNC( uint32, MCU_CODE) Mcu_PMC_GetInterruptStatus(VAR( void, AUTOMATIC));
#endif /* (MCU_VOLTAGE_ERROR_ISR_USED == STD_ON) */

#if (MCU_TEMPERATURE_ERROR_ISR_USED == STD_ON)
FUNC( void, MCU_CODE) Mcu_PMC_TemperatureErrorIsr( VAR( void, AUTOMATIC) );
#endif /* ( (MCU_TEMPERATURE_ERROR_ISR_USED == STD_ON) ) */


#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

#endif /* MCU_PMC_H */

/** @} */
