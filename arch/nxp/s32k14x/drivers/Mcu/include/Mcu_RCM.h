/**
*   @file    Mcu_RCM.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Function prototypes.
*   @details Interface available for IPW layer only.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_RCM_H
#define MCU_RCM_H


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_RCM_Types.h"
#include "Mcu_EnvCfg.h"

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_RCM_SystemResetIsrConfig() \
do\
{ \
        Mcal_Trusted_Call(Mcu_RCM_SystemResetIsrConfig); \
}\
while(0)
#else
#define  Call_Mcu_RCM_SystemResetIsrConfig() \
do\
{ \
        Mcu_RCM_SystemResetIsrConfig(); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_RCM_RestoreSystemResetIsrConfig(u32SystemResetIsrConfig) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_RCM_RestoreSystemResetIsrConfig,(u32SystemResetIsrConfig)); \
}\
while(0)
#else
#define  Call_Mcu_RCM_RestoreSystemResetIsrConfig(u32SystemResetIsrConfig) \
do\
{ \
        Mcu_RCM_RestoreSystemResetIsrConfig(u32SystemResetIsrConfig); \
}\
while(0)
#endif
#endif

#endif /* (MCU_ENTER_LOW_POWER_MODE == STD_ON) */

#if (defined(MCU_DISABLE_RCM_INIT) && (STD_OFF == MCU_DISABLE_RCM_INIT))
#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_RCM_ResetInit(RCM_pResetConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_RCM_ResetInit,(RCM_pResetConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_RCM_ResetInit(RCM_pResetConfigPtr) \
do\
{ \
        Mcu_RCM_ResetInit(RCM_pResetConfigPtr); \
}\
while(0)
#endif
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_RCM_GetResetReason() \
        Mcal_Trusted_Call_Return(Mcu_RCM_GetResetReason)
#else
#define  Call_Mcu_RCM_GetResetReason() \
        Mcu_RCM_GetResetReason()
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_RCM_GetResetRawValue() \
        Mcal_Trusted_Call_Return(Mcu_RCM_GetResetRawValue)
#else
#define  Call_Mcu_RCM_GetResetRawValue() \
        Mcu_RCM_GetResetRawValue()
#endif
#endif


#if (defined(MCU_DISABLE_RCM_INIT) && (STD_OFF == MCU_DISABLE_RCM_INIT))
FUNC(void, MCU_CODE) Mcu_RCM_ResetInit(P2CONST( Mcu_RCM_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
#endif

FUNC( Mcu_ResetType, MCU_CODE) Mcu_RCM_GetResetReason( VAR( void, AUTOMATIC));

FUNC( Mcu_RawResetType, MCU_CODE) Mcu_RCM_GetResetRawValue( VAR( void, AUTOMATIC));

#ifdef MCU_ERROR_ISR_NOTIFICATION
#if (defined(MCU_RESET_ALTERNATE_ISR_USED) && (MCU_RESET_ALTERNATE_ISR_USED == STD_ON))
FUNC(void, MCU_CODE) Mcu_RCM_ResetAltInt(void);
#endif /* (MCU_RESET_ALTERNATE_ISR_USED == STD_ON) */
#endif /* (MCU_ERROR_ISR_NOTIFICATION) */

#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)
FUNC( uint32, MCU_CODE) Mcu_RCM_GetCurrentSystemResetIsrSettings( VAR( void, AUTOMATIC));
FUNC( void, MCU_CODE) Mcu_RCM_SystemResetIsrConfig( VAR( void, AUTOMATIC));
FUNC( void, MCU_CODE) Mcu_RCM_RestoreSystemResetIsrConfig( VAR( uint32, AUTOMATIC ) u32SystemResetIsrConfig);
#endif


#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

#endif /* MCU_RCM_H */

/** @} */
