/**
*   @file    Mcu_PMC_IPVersion.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Platform version definitions, used inside IPV_PMC.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/

#ifndef MCU_PMC_IPVERSION_H
#define MCU_PMC_IPVERSION_H


#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/

/* Define IP version */
#define IPV_PMC_00_00_01_20      (0x00000120UL)
#define IPV_PMC_00_00_00_15      (0x00000015UL)
#define IPV_PMC_03_00_01_00      (0x03000100UL)
#define IPV_PMC_01_00_02_05      (0x01000205UL)

#ifdef __cplusplus
}
#endif

#endif /* MCU_PMC_IPVERSION_H */
