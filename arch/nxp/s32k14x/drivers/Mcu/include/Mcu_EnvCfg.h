/**
*   @file    Mcu_EnvCfg.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Driver external interface.
*   @details Contains all the public functions and data types that are used by the higher layer.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_SCFG_H
#define MCU_SCFG_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/
/* External environment the Mcu driver currently supports */
#define MCU_FTE (STD_ON)

/* FTE environment */
#define MCU_PERIODIC_CHECKS             (MCU_FTE)
#define MCU_PARAM_CHECK                 (MCU_DEV_ERROR_DETECT)

/* high level defines */
#define MCU_VALIDATE_GLOBAL_CALL        (MCU_DEV_ERROR_DETECT)
#define MCU_USER_MODE_REG_PROT_ENABLED  (STD_OFF)

#define MCU_PARAM_UNUSED(param)         ((void)((param)))


#ifdef __cplusplus
}
#endif

#endif /* MCU_SCFG_H */

/** @} */
