/**
*   @file    Mcu_SIM.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - System Mode Controller functions implementation.
*   @details Specific functions for SIM configuration and control.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_SIM.h"
#include "Reg_eSys_SIM.h"

#if (MCU_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /* (MCU_DEV_ERROR_DETECT == STD_ON) */

#ifndef USER_MODE_REG_PROT_ENABLED
#define USER_MODE_REG_PROT_ENABLED ( MCU_USER_MODE_REG_PROT_ENABLED )
#endif

#include "SilRegMacros.h"

/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/
#if (defined(MCU_DISABLE_SIM_INIT) && (STD_OFF == MCU_DISABLE_SIM_INIT))
/**
* @brief            This function configure the System Integration Module (SIM).
* @details          This routine initializes the SIM module that provides system control
*                   and chip configuration registers.
*                   Called by:
*                       - Mcu_IPW_Init() from IPW.
*
* @param[in]        pConfigPtr   Pointer to SIM configuration structure.
*
* @return           void
*
*/

FUNC(void, MCU_CODE) Mcu_SIM_Init(P2CONST(Mcu_SIM_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    /* Configure SIM_CHIPCTL general settings (COCO_TRG_SEL, PDB_BB_SEL, DAC2CMP_SEL, ADC_INTERLEAVE_SEL) */
    REG_RMW32(SIM_CHIPCTL_ADDR32, SIM_CHIPCTL_INIT_MASK32, (uint32)pConfigPtr->u32ChipControlConfiguration);
    /* Configure SIM_LPOCLKS register that is a write once register */
    REG_WRITE32(SIM_LPOCLKS_ADDR32, ((uint32)pConfigPtr->u32LPOClockConfiguration & SIM_LPOCLKS_RWBITS_MASK32));
    /* Configure SIM_ADCOPT register */
    REG_WRITE32(SIM_ADCOPT_ADDR32, ((uint32)pConfigPtr->u32AdcOptionsConfiguration & SIM_ADCOPT_RWBITS_MASK32));
    /* Configure SIM_FTMOPT0 and SIM_FTMOPT1 registers */
    REG_WRITE32(SIM_FTMOPT0_ADDR32, ((uint32)pConfigPtr->u32FTMOPT0Configuration & SIM_FTMOPT0_RWBITS_MASK32));
    REG_WRITE32(SIM_FTMOPT1_ADDR32, ((uint32)pConfigPtr->u32FTMOPT1Configuration & SIM_FTMOPT1_RWBITS_MASK32));
    /* Configure SIM_MISCTRL0 register */
    REG_WRITE32(SIM_MISCTRL0_ADDR32, ((uint32)pConfigPtr->u32MiscellaneousConfiguration0 & SIM_MISCTRL0_RWBITS_MASK32));
    /* Configure SIM_MISCTRL1 register */
    REG_WRITE32(SIM_MISCTRL1_ADDR32, ((uint32)pConfigPtr->u32MiscellaneousConfiguration1 & SIM_MISCTRL1_RWBITS_MASK32));
}
#endif /* #if (defined(MCU_DISABLE_SIM_INIT) && (STD_OFF == MCU_DISABLE_SIM_INIT)) */

#if (MCU_INIT_CLOCK == STD_ON)
/**
* @brief            SIM clock initialization function.
* @details          This function intializes the clock that is controled by SIM module.
*                   Called by:
*                       - Mcu_Ipw_InitClock() from IPW.
*
* @param[in]        pClockConfigPtr   Pointer to SIM configuration structure.
*
* @return           void
*
*/
FUNC( void, MCU_CODE) Mcu_SIM_ClockConfig(P2CONST(Mcu_SIM_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pClockConfigPtr)
{
    /* Configure PLATGC register */
    REG_WRITE32(SIM_PLATCGC_ADDR32, ((uint32)pClockConfigPtr->u32ClockGatingConfiguration & SIM_PLATGC_RWBITS_MASK32));
    
    /* Configure SIM_CHIPCTL clock settings (TRACECLK_SEL, CLKOUTEN, CLKOUTDIV, CLKOUT_SEL) */
    REG_RMW32(SIM_CHIPCTL_ADDR32, SIM_CHIPCTL_CLOCK_MASK32, ((uint32)pClockConfigPtr->u32ChipControlClockConfiguration & (~SIM_CHIPCTL_CLKOUTEN_MASK32)));
    /* Enable Clockout */
    REG_RMW32(SIM_CHIPCTL_ADDR32, SIM_CHIPCTL_CLKOUTEN_MASK32, ((uint32)pClockConfigPtr->u32ChipControlClockConfiguration & SIM_CHIPCTL_CLKOUTEN_MASK32));
    
    /* Configure Trace Clock settings */
    REG_WRITE32(SIM_CLKDIV4_ADDR32, ((uint32)pClockConfigPtr->u32TraceClockConfiguration & SIM_CLKDIV4_RWBITS_MASK32));
}
#endif

#ifdef MCU_SRAM_RETEN_CONFIG_API
#if (MCU_SRAM_RETEN_CONFIG_API == STD_ON)
/**
 * @brief   Configuration for SRAM retention.
 * @details This function configure for both SRAML_RETEN and SRAMU_RETEN bits.
 *                   Called by:
 *                       - Mcu_Ipw_SRAMRetentionConfig() from IPW
 * @param[in] eSRAMRetenConfig    The value will be configured to SRAML_RETEN and SRAMU_RETEN bits.
 *                                  MCU_SRAML_RETEN - SRAML will be retain only.
 *                                  MCU_SRAMU_RETEN - SRAMU will be retain only.
 *                                  MCU_SRAMLU_RETEN - Both SRAML and SRAMU will be retain.
 *                                  MCU_NO_SRAMLU_RETEN - Both SRAML and SRAMU will not be retain.
 *
 * @return                   void
 *
 */
FUNC( void, MCU_CODE ) Mcu_SIM_SRAMRetentionConfig(VAR (Mcu_SRAMRetenConfigType, AUTOMATIC) eSRAMRetenConfig)
{
#ifdef MCAL_PLATFORM_ARM
    /* add dsb to guarantee that there are no delayed writes to SRAM */
    ASM_KEYWORD("dsb");
#endif
    /* Configure to both SRAML_RETEN and SRAMU_RETEN bits */
    REG_RMW32(SIM_CHIPCTL_ADDR32,(SIM_CHIPCTL_SRAML_RETEN_MASK32 | SIM_CHIPCTL_SRAMU_RETEN_MASK32), (uint32)eSRAMRetenConfig);
        
#ifdef MCAL_PLATFORM_ARM
        ASM_KEYWORD("dsb");
#endif
}
#endif
#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
