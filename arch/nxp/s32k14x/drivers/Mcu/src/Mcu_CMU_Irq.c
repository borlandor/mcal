/**
*   @file    Mcu_CMU_Irq.c
*
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Interrupt handlers.
*   @details Interrupt handlers that should be managed by the Mcu driver.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcal.h"
#include "Mcu_CMU.h"


/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#ifdef MCU_CMU_ERROR_ISR_USED
#if (MCU_CMU_ERROR_ISR_USED == STD_ON)
ISR( Mcu_Cmu_ClockFailIsr );
#endif /* (MCU_CMU_ERROR_ISR_USED == STD_ON) */
#endif

/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/

#ifdef MCU_CMU_ERROR_ISR_USED
#if (MCU_CMU_ERROR_ISR_USED == STD_ON)
/**
* @brief       This function represents the ISR handler for CMU related events.
* @details     This interrupt is triggered if one of the enabled CMU's detect an out of range clock.
*
*
* @isr
*
*/
/** @violates @ref Mcu_CMU_IRQ_c_REF_5 MISRA 2004 The IRQ handler is used by external code */
#if (IPV_CMU == IPV_CMU_00_00_00_01)
/* Interrupt function call by Mcu_IPW layer with name Mcu_PMC_SCG_CMU_Isr */
#else
ISR( Mcu_Cmu_ClockFailIsr )
{
    Mcu_CMU_ClockFailInt();
    EXIT_INTERRUPT();
}
#endif
#endif /* (MCU_CMU_ERROR_ISR_USED == STD_ON) */
#endif


#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
