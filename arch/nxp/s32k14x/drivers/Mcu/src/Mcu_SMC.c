/**
*   @file    Mcu_SMC.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - System Mode Controller functions implementation.
*   @details Specific functions for SMC configuration and control.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_SMC.h"

#include "Reg_eSys_SMC.h"

#if (MCU_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /* (MCU_DEV_ERROR_DETECT == STD_ON) */

#ifndef USER_MODE_REG_PROT_ENABLED
#define USER_MODE_REG_PROT_ENABLED ( MCU_USER_MODE_REG_PROT_ENABLED )
#endif

#include "SilRegMacros.h"


/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/

#if (defined(MCU_DISABLE_SMC_INIT) && (STD_OFF == MCU_DISABLE_SMC_INIT))
/**
* @brief            This function will configure the allowed modes
*
* @details          This function is only called at Mcu initialization
*
*/
FUNC( void, MCU_CODE) Mcu_SMC_AllowedModesConfig(P2CONST(Mcu_SMC_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    REG_WRITE32(SMC_PMPROT_ADDR32, (pConfigPtr->u32AllowedModes & SMC_PMPROT_RWBITS_MASK32));
}
#endif /* #if (defined(MCU_DISABLE_SMC_INIT) && (STD_OFF == MCU_DISABLE_SMC_INIT)) */


/**
* @brief            This function switches the mode by writing SMC_PMCTRL and SMC_STOPCTRL.
* @details          This function configures the mode settings by writing the 
*                   SMC_PMCTRL and SMC_STOPCTRL registers.
*
*                   Called by:
*                       - Mcu_IPW_SetMode() from IPW.
*
* @param[in]        pModeConfigPtr   Pointer to mode configuration structure.
*
* @return           void
*
*/
FUNC( void, MCU_CODE) Mcu_SMC_ModeConfig(P2CONST( Mcu_ModeConfigType, AUTOMATIC, MCU_APPL_CONST) pModeConfigPtr)
{
    /* Contains the code of the Power Mode (based PowerModeType enumeration). */
    VAR( Mcu_PowerModeType, AUTOMATIC) ePowerMode = MCU_RUN_MODE;
    /* Contains the code of the Power Mode (based PowerModeType enumeration). */
    VAR( uint32, AUTOMATIC) eCurrentPowerMode = SMC_PMSTAT_RUN_U32;
#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)  
    /* Contains value of latest written register before executing WFI instruction */
    VAR( uint32, AUTOMATIC) u32RegValue = (uint32)0U;
#endif
    
    eCurrentPowerMode = REG_READ32(SMC_PMSTAT_ADDR32) & SMC_PMSTAT_PMSTAT_MASK32;
    
    ePowerMode = pModeConfigPtr->u32PowerMode;
    
    switch (ePowerMode)
    {
        case (MCU_RUN_MODE):
        {
            /* Set RUN mode */
            REG_RMW32(SMC_PMCTRL_ADDR32, SMC_PMCTRL_RUNM_MASK32, SMC_PMCTRL_RUN_MODE_U32);

            break;
        }
        case (MCU_HSRUN_MODE):
        {
            if (SMC_PMSTAT_RUN_U32 == eCurrentPowerMode)
            {
                /* Set HSRUN mode. */
                REG_RMW32(SMC_PMCTRL_ADDR32, SMC_PMCTRL_RUNM_MASK32, SMC_PMCTRL_HSRUN_MODE_U32);
            }
            
            break;
        }      
        case (MCU_VLPR_MODE):
        {
            if ((SMC_PMSTAT_RUN_U32 == eCurrentPowerMode) || (SMC_PMSTAT_VLPS_U32 == eCurrentPowerMode))
            {
                /* Set VLPR mode. */
                REG_RMW32(SMC_PMCTRL_ADDR32, SMC_PMCTRL_RUNM_MASK32, SMC_PMCTRL_VLPR_MODE_U32);
            }
            
            break;
        }

#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)  
        case (MCU_VLPS_MODE):
        {
            if ((SMC_PMSTAT_RUN_U32 == eCurrentPowerMode) || (SMC_PMSTAT_VLPR_U32 == eCurrentPowerMode))
            {
                /* Set VLPS mode. */
                REG_RMW32(SMC_PMCTRL_ADDR32, SMC_PMCTRL_STOPM_MASK32, SMC_PMCTRL_VLPS_U32);
                /* This read ensure transistion mode successful before executing WFI instruction */
                u32RegValue = REG_READ32(SMC_PMCTRL_ADDR32);

                EXECUTE_WAIT();
            }
            break;
        }
        case (MCU_STOP1_MODE):
        {
            if (SMC_PMSTAT_RUN_U32 == eCurrentPowerMode)
            {
                /* Set STOP1 mode. */
                REG_WRITE32(SMC_STOPCTRL_ADDR32, SMC_STOPCTRL_STOP1_U32);
                REG_RMW32(SMC_PMCTRL_ADDR32, SMC_PMCTRL_STOPM_MASK32, SMC_PMCTRL_STOP_U32);
                /* This read ensures the transition was successful before executing WFI instruction */
                u32RegValue = REG_READ32(SMC_STOPCTRL_ADDR32);
                    
                EXECUTE_WAIT();
            }
            break;
        }
        case (MCU_STOP2_MODE):
        {
            if (SMC_PMSTAT_RUN_U32 == eCurrentPowerMode)
            {
                /* Set STOP2 mode. */
                REG_WRITE32(SMC_STOPCTRL_ADDR32, SMC_STOPCTRL_STOP2_U32);
                REG_RMW32(SMC_PMCTRL_ADDR32, SMC_PMCTRL_STOPM_MASK32, SMC_PMCTRL_STOP_U32);
                /* This read ensures the transition was successful before executing WFI instruction */
                u32RegValue = REG_READ32(SMC_STOPCTRL_ADDR32);
            
                EXECUTE_WAIT();
            }
            break;
        }
#endif        
        default:
        {
            /*Only the above modes are allowed when this function is called*/
            break;
        }
    }

#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)
    /* Avoid compiler warning */
    MCU_PARAM_UNUSED(u32RegValue);
#endif
}

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
