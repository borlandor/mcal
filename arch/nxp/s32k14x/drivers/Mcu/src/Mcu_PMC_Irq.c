/**
*   @file    Mcu_PMC_Irq.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Power Management Controller module functions implementation.
*   @details Specific functions for PMC configuration and control.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/

#ifdef __cplusplus
extern "C"
{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcal.h"
#include "Mcu_PMC.h"

/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#if (MCU_VOLTAGE_ERROR_ISR_USED == STD_ON)
ISR(Mcu_PMC_LowVoltage_ISR);
#endif /* (MCU_VOLTAGE_ERROR_ISR_USED == STD_ON) */

#if (MCU_TEMPERATURE_ERROR_ISR_USED == STD_ON)
ISR(Mcu_PMC_TemperatureError_ISR);
#endif /* (MCU_TEMPERATURE_ERROR_ISR_USED == STD_ON) */



/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/

#if (MCU_VOLTAGE_ERROR_ISR_USED == STD_ON)
/**
* @brief       This function represents the ISR handler for PMC related events.
* @details     This interrupt is triggered if one of the enabled PMC's detect an out of range clock.
*
*
* @isr
*
*/
/** @violates @ref Mcu_PMC_IRQ_c_REF_5 MISRA 2004 The IRQ handler is used by external code */
#ifndef MCU_CMU_PMC_SCG_INTERRUPT
ISR(Mcu_PMC_LowVoltage_ISR)
{
    Mcu_PMC_VoltageErrorIsr();
    EXIT_INTERRUPT();
}
#else 
#if(MCU_CMU_PMC_SCG_INTERRUPT == STD_OFF)
ISR(Mcu_PMC_LowVoltage_ISR)
{
    Mcu_PMC_VoltageErrorIsr();
    EXIT_INTERRUPT();
}
#endif
#endif
#endif /* (MCU_VOLTAGE_ERROR_ISR_USED == STD_ON) */

#if (MCU_TEMPERATURE_ERROR_ISR_USED == STD_ON)
/**
* @brief       This function represents the ISR handler for PMC related events.
* @details     This interrupt is triggered if one of the enabled PMC's detect an out of range clock.
*
*
* @isr
*
*/
/** @violates @ref Mcu_PMC_IRQ_c_REF_5 MISRA 2004 The IRQ handler is used by external code */
ISR(Mcu_PMC_TemperatureError_ISR)
{
    Mcu_PMC_TemperatureErrorIsr();
    EXIT_INTERRUPT();
}
#endif /* (MCU_TEMPERATURE_ERROR_ISR_USED == STD_ON) */


#define MCU_STOP_SEC_CODE
/**
* @violates @ref Mcu_PMC_IRQ_c_REF_1 MISRA 2004 Required Rule 19.15, Repeated include file
* @violates @ref Mcu_PMC_IRQ_c_REF_2 MISRA 2004 Only preprocessor statements and comments before
* '#include'
*/
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
