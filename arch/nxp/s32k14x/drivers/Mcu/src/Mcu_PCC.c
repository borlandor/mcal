/**
*   @file    Mcu_PCC.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Peripheral Clock Control functions implementation.
*   @details Specific functions for PCC configuration and control.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_PCC.h"

#include "Reg_eSys_PCC.h"

#if (MCU_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /* (MCU_DEV_ERROR_DETECT == STD_ON) */

#ifndef USER_MODE_REG_PROT_ENABLED
#define USER_MODE_REG_PROT_ENABLED ( MCU_USER_MODE_REG_PROT_ENABLED )
#endif

#include "SilRegMacros.h"


/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#if (MCU_INIT_CLOCK == STD_ON)

/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/

/**
* @brief            This function will configure the peripherals
*
* @details          This function will configure the peripherals
*
*/
FUNC( void, MCU_CODE) Mcu_PCC_PeripheralConfig(P2CONST(Mcu_PCC_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    VAR(uint32, AUTOMATIC) u32Counter;
    VAR(uint32, AUTOMATIC) u8PeripheralIsPresent;
        
    for ( u32Counter = (uint32)0x00U; u32Counter < (uint32)(pConfigPtr->u32NumberOfPccRegisters); u32Counter++)
    {
        u8PeripheralIsPresent = REG_READ32((*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterAddr) & PCC_PR_MASK32;
        
        if (PCC_PERIPHERAL_IS_PRESENT_U32 == u8PeripheralIsPresent)
        {
            if(PCC_PCS_UNAVAILABLE_U32 != ((*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterData & PCC_PCS_UNAVAILABLE_U32))
            {
                /* Disable Peripheral clock first */
                REG_BIT_CLEAR32((*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterAddr,PCC_CGC_MASK32);
                /* Configure all parameters */
                REG_WRITE32((*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterAddr, (*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterData);
                
            }
            else
            {
                REG_WRITE32((*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterAddr, (*pConfigPtr->apPeripheralConfig)[u32Counter].u32RegisterData);
            }
        }
    }
    
}
#endif

#ifdef MCU_ENABLE_CMU_PERIPHERAL
#if(MCU_ENABLE_CMU_PERIPHERAL == STD_ON)
FUNC( uint32, MCU_CODE) Mcu_PCC_GetStatus(VAR(uint32, AUTOMATIC) u32RegisterAddr)
{
    return REG_READ32(u32RegisterAddr);
}
#endif
#endif
    
#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
