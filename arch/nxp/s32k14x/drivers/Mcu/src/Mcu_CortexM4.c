/**
*   @file    Mcu_CortexM4.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - ARM CortexM4 Registers and Macros Definitions.
*   @details ARM CortexM4 Registers and Macros Definitions.
*
*   @addtogroup CORTEXM_DRIVER
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/

/**
@file        Mcu_CortexM4.c
*/


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_Cfg.h"
#include "Mcu_CortexM4.h"
#include "StdRegMacros.h"


#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#if (MCU_PERFORM_RESET_API == STD_ON)
 /**
* @brief        The function initiates a system reset request to reset the SoC.
* @details      The function initiates a system reset request to reset the SoC
*
* @param[in]    none
*
* @return void
*
*/
FUNC(void, MCU_CODE) Mcu_CM4_SystemReset(void)
{

    ASM_KEYWORD(" dsb");               /* All memory accesses have to be completed before reset */
    REG_WRITE32( CM4_AIRCR_BASEADDR, (uint32)(CM4_AIRCR_VECTKEY(0x5FAU) | (REG_READ32(CM4_AIRCR_BASEADDR) & CM4_AIRCR_PRIGROUP_MASK) | CM4_AIRCR_SYSRESETREQ_MASK ));
    ASM_KEYWORD(" dsb");               /* All memory accesses have to be completed */

}

#endif


 /**
* @brief        The function initiates a system reset request to reset the SoC.
* @details      The function initiates a system reset request to reset the SoC
*
* @param[in]    none
*
* @return void
*/
FUNC(void, MCU_CODE) Mcu_CM4_EnableDeepSleep(void)
{
    /** @violates @ref Mcu_CortexM4_c_REF_5 The cast is used to access memory mapped registers.*/
    REG_RMW32( CM4_SCR_BASEADDR, CM4_SCR_SLEEPDEEP_MASK32, CM4_SCR_SLEEPDEEP_U32);

}

 /**
* @brief        The function initiates a system reset request to reset the SoC.
* @details      The function initiates a system reset request to reset the SoC
*
* @param[in]    none
*
* @return void
*/
FUNC(void, MCU_CODE) Mcu_CM4_DisableDeepSleep(void)
{
    /** @violates @ref Mcu_CortexM4_c_REF_5 The cast is used to access memory mapped registers.*/
    REG_RMW32( CM4_SCR_BASEADDR, CM4_SCR_SLEEPDEEP_MASK32, (uint32)0U);

}


#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"

#ifdef __cplusplus
}
#endif



/** @} */
