/**
*   @file    Mcu_IPW.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Middle layer implementation.
*   @details Layer that implements the wrapper for routing data from/to external interface
*            to IP layer.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_IPW.h"
#include "Mcu_IPW_Irq.h"
#include "Mcu_PMC.h"
#include "Mcu_SCG.h"
#include "Mcu_CMU.h"


/*==================================================================================================
                                       LOCAL VARIABLES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#if (MCU_CMU_PMC_SCG_INTERRUPT == STD_ON)
/**
* @brief       This function represents the ISR handler for CMU PMC SCG related events.
* @details     This interrupt is triggered if one of the enabled CMU PMC SCG detect an out of range clock.
*              this function exist only on S32K11X.
*
*
* @isr
*
*/
ISR( Mcu_PMC_SCG_CMU_Isr )
{
#ifdef MCU_ENABLE_CMU_PERIPHERAL
  #if(STD_ON == MCU_ENABLE_CMU_PERIPHERAL)
    if (Mcu_CMU_GetInterruptStatus(CMU1_CHANNEL) != 0U)
    {
  #ifdef MCU_CMU_ERROR_ISR_USED
    #if (STD_ON == MCU_CMU_ERROR_ISR_USED)
        /* CMU1 interrupt */
        Mcu_CMU_ClockFailInt();
    #endif
  #endif
    }
  #endif
#endif

#ifdef MCU_VOLTAGE_ERROR_ISR_USED
  #if (STD_ON == MCU_VOLTAGE_ERROR_ISR_USED)
    if (Mcu_PMC_GetInterruptStatus() != 0U)
    {
        /* PMC interrupt */
        Mcu_PMC_VoltageErrorIsr();
    }
  #endif
#endif
   
    /* SCG Interrupt */
/* if(Mcu_SCG_GetInterruptStatus() != 0U)
    {
        
    }
*/
    EXIT_INTERRUPT();
}

#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */

